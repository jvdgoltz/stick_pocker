from distutils.core import setup

setup(name='Stick Pocker',
      version='0.1.0',
      description='Stock picker hobby project',
      author='Julian von der Goltz',
      author_email='j.vdgoltz@gmail.com',
      url='https://www.gitlab.com/jvdgoltz/stick_pocker',
      packages=['distutils', 'distutils.command'],
     )
